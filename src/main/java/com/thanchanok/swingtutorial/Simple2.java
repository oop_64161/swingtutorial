package com.thanchanok.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyFrame extends JFrame {
    JButton button;
    public MyFrame() {
        super("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400, 500);  
    }
}

public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setVisible(true);
    }
}
